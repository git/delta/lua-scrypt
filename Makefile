ifeq ($(strip $(shell which pkg-config > /dev/null || echo NONE)),NONE)
  $(error This makefile requires pkg-config)
endif

ifeq ($(strip $(shell which $(CC) > /dev/null || echo NONE)),NONE)
  $(info lua-scrypt: Hmm, CC make variable contains nonsense.  Overriding to gcc.)
  CC := gcc
endif

NAKED_LUA_VER := $(shell (pkg-config --exists lua && \
                          pkg-config --modversion lua || \
			  pkg-config --variable V lua) | cut -d. -f1-2)

# fine appropriate Lua interpreter
LUA_REPL := $(shell (which lua5.1 > /dev/null 2>&1 && echo lua5.1) || \
                    (which lua-5.1 > /dev/null 2>&1 && echo lua-5.1) || \
                    (which lua51  > /dev/null 2>&1 && echo lua51) || \
                    (lua -e"print(_VERSION)" | grep -q 5.1 && echo lua) || \
                    (which lua-5.2 > /dev/null 2>&1 && echo lua-5.2) || \
                    (which lua52 > /dev/null 2>&1 && echo lua52))

# find appropriate Lua libraries
LUA51_PKG := $(shell (pkg-config --exists lua5.1 && echo lua5.1) || \
                     (pkg-config --exists lua-5.1 && echo lua-5.1) || \
		     (pkg-config --exists lua51 && echo lua51) || \
                     (echo "NONE"))


LUA51_PKG := $(strip $(LUA51_PKG))

ifeq ($(LUA51_PKG),NONE)
  ifeq ($(NAKED_LUA_VER),5.1)
    LUA51_PKG := lua
  endif
endif

ifneq ($(LUA51_PKG),NONE)
  $(info lua-scrypt: Lua 5.1 package name on this system is $(LUA51_PKG))
  LUA51_HAS_MODINST := $(shell test x`pkg-config --variable INSTALL_LMOD $(LUA51_PKG)` != x && echo YES)
  LUA51_INC := $(shell pkg-config --cflags $(LUA51_PKG))
  LUA51_LIB := $(shell pkg-config --libs $(LUA51_PKG))
  ifeq ($(LUA51_HAS_MODINST),)
    LOCAL := YES
  endif
  ifeq ($(LOCAL),)
    LUA51_LMOD_INST := $(shell pkg-config --variable=INSTALL_LMOD $(LUA51_PKG))
    LUA51_CMOD_INST := $(shell pkg-config --variable=INSTALL_CMOD $(LUA51_PKG))
  else
    LUA51_LMOD_INST := /usr/local/share/lua/5.1
    LUA51_CMOD_INST := /usr/local/lib/lua/5.1
  endif
  lua-5.1-try: lua-5.1
  lua-5.1-try-install: lua-5.1-install
else
lua-5.1-try:
	@echo lua-scrypt: Lua 5.1 could not be found, so lua-scrypt was not built for it.
lua-5.1-try-install:
	@echo lua-scrypt: Lua 5.1 could not be found, so lua-scrypt was not installed for it.
endif

LUA52_PKG := $(shell (pkg-config --exists lua5.2 && echo lua5.2) || \
                     (pkg-config --exists lua-5.2 && echo lua-5.2) || \
                     (pkg-config --exists lua52 && echo lua52) || \
                     (echo "NONE")) 

LUA52_PKG := $(strip $(LUA52_PKG))

ifeq ($(LUA52_PKG),NONE)
  ifeq ($(NAKED_LUA_VER),5.2)
    LUA52_PKG := lua
  endif
endif

ifneq ($(LUA52_PKG),NONE)
  $(info lua-scrypt: Lua 5.2 package name on this system is $(LUA52_PKG))
  LUA52_HAS_MODINST := $(shell test x`pkg-config --variable INSTALL_LMOD $(LUA52_PKG)` != x && echo YES)
  LUA52_INC := $(shell pkg-config --cflags $(LUA52_PKG))
  LUA52_LIB := $(shell pkg-config --libs $(LUA52_PKG))
  ifeq ($(LUA52_HAS_MODINST),)
    LOCAL := YES
  endif
  ifeq ($(LOCAL),)
    LUA52_LMOD_INST := $(shell pkg-config --variable=INSTALL_LMOD $(LUA52_PKG))
    LUA52_CMOD_INST := $(shell pkg-config --variable=INSTALL_CMOD $(LUA52_PKG))
  else
    LUA52_LMOD_INST := /usr/local/share/lua/5.2
    LUA52_CMOD_INST := /usr/local/lib/lua/5.2
  endif
  lua-5.2-try: lua-5.2
  lua-5.2-try-install: lua-5.2-install
else
lua-5.2-try:
	@echo lua-scrypt: Lua 5.2 could not be found, so lua-scrypt was not built for it.
lua-5.2-try-install:
	@echo lua-scrypt: Lua 5.2 could not be found, so lua-scrypt was not installed for it.
endif

LIBCRYPT_C := lib/crypto/crypto_aesctr.c \
			   lib/crypto/crypto_scrypt-nosse.c \
			   lib/crypto/crypto_scrypt-ref.c \
			   lib/crypto/sha256.c

ifneq ($(USE_SSE),)
	 LIBCRYPT_C := $(subst -nosse.c$,-sse.c,$(LIBCRYPT_C))
endif

LIBCRYPT_O := $(subst .c$,.o,$(LIBCRYPT_C))

LIBSCRYPTENC_C := lib/scryptenc/scryptenc.c lib/scryptenc/scryptenc_cpuperf.c
LIBSCRYPTENC_O := $(subst .c$,.o,$(LIBSCRYPTENC_C))

LIBUTIL_C := lib/util/memlimit.c lib/util/readpass.c lib/util/warn.c
LIBUTIL_O := $(subst .c$,.o,$(LIBUTIL_C))

SCRYPT_LIBS := libcrypt.a libscryptenc.a libutil.a

CFLAGS ?= -O2 -Wall
INSTALL := /usr/bin/install

all: lua-5.1-try lua-5.2-try

clean:
	 $(RM) scrypt-5.1.so scrypt-5.2.so scrypt.so
	 $(RM) $(SCRYPT_LIBS)
	 $(RM) $(LIBCRYPT_O)
	 $(RM) $(LIBSCRYPTENC_O)
	 $(RM) $(LIBUTIL_O)

%.o: %.c
	 $(CC) $(CFLAGS) -fPIC $(LUA51_INC) -I lib/util -I lib/crypto -I lib/scryptenc -c $< -o $@

libcrypt.a: $(LIBCRYPT_O)
	 $(AR) q libcrypt.a $(LIBCRYPT_O)

libscryptenc.a: $(LIBSCRYPTENC_O)
	 $(AR) q libscryptenc.a $(LIBSCRYPTENC_O)

libutil.a: $(LIBUTIL_O)
	 $(AR) q libutil.a $(LIBUTIL_O)

lua-5.1: scrypt-5.1.so
	 ln -s -f scrypt-5.1.so scrypt.so

lua-5.1-install: lua-5.1
	 $(INSTALL) -d $(DESTDIR)$(LUA51_CMOD_INST)
	 $(INSTALL) -m 755 scrypt-5.1.so $(DESTDIR)$(LUA51_CMOD_INST)/scrypt.so

scrypt-5.1.so: luascrypt.o $(SCRYPT_LIBS)
	 $(CC) $(CFLAGS) -shared -o $@ $^ $(LUA51_LIB) $(SCRYPT_LIBS)

lua-5.2: scrypt-5.2.so
	 ln -s -f scrypt-5.2.so scrypt.so

lua-5.2-install: lua-5.2
	 $(INSTALL) -d $(DESTDIR)$(LUA52_CMOD_INST)
	 $(INSTALL) -m 755 scrypt-5.2.so $(DESTDIR)$(LUA52_CMOD_INST)/scrypt.so

scrypt-5.2.so: luascrypt.o $(SCRYPT_LIBS)
	 $(CC) $(CFLAGS) -shared -o $@ $^ $(LUA51_LIB) $(SCRYPT_LIBS)


