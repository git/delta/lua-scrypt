local scrypt = require "scrypt"

local curtime = 0;

local function time(m)
	curtime = os.clock() - curtime;
	io.stdout:write(m, ": ",curtime, "\n")
end

local hash1 = scrypt.hash_password("Hello", 2^14, 8, 1)
time "Generate hash1"
local hash2 = scrypt.hash_password("Hello", 2^14, 8, 1)
time "Generate hash2"

assert(hash1 ~= hash2) -- hashes are salted

local hash3 = scrypt.hash_password("Hello", 2^16, 8, 1)
time "Generate hash3"

assert(scrypt.verify_password(hash1, "Hello"))
time "Verify hash1 with correct password"

assert(scrypt.verify_password(hash2, "World") == false)
time "Verify hash2 with incorrect password"

assert(scrypt.verify_password(hash3, "Hello"))
time "Verify hash3 with correct password"

assert(scrypt.verify_password(hash3, "World") == false)
time "Verify hash3 with incorrect password"
